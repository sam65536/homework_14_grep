package com.geekhub.samiilenko.grep;

import org.apache.commons.cli.*;

public class CommandLineValidator {

    private GrepMenu menu = new GrepMenu();

    public boolean isValid(String[] arguments) throws InvalidParametersException {
        CommandLine commandLine = getCommandLineFromString(arguments);
        boolean result = false;
        Option[] options = commandLine.getOptions();
        if ((options.length == 1)
                && (options[0].getOpt().equalsIgnoreCase("h"))) {
            result = true;
        }
        if ((options.length == 2)
                && (options[0].getOpt().equalsIgnoreCase("w"))
                && (options[1].getOpt().equalsIgnoreCase("f"))) {
            result = true;
        }
        if ((options.length == 2)
                && (options[0].getOpt().equalsIgnoreCase("w"))
                && (options[1].getOpt().equalsIgnoreCase("u"))) {
            result = true;
        }
        if ((options.length == 2)
                && (options[0].getOpt().equalsIgnoreCase("r"))
                && (options[1].getOpt().equalsIgnoreCase("f"))) {
            result = true;
        }
        if ((options.length == 2)
                && (options[0].getOpt().equalsIgnoreCase("r"))
                && (options[1].getOpt().equalsIgnoreCase("u"))) {
            result = true;
        }
        return result;
    }

    CommandLine getCommandLineFromString (String[] arguments) throws InvalidParametersException {
        CommandLineParser commandLineParser = new DefaultParser();
        CommandLine commandLine = null;
        try {
            commandLine = commandLineParser.parse(menu.getOptions(), arguments);
        } catch (ParseException e) {
            throw new InvalidParametersException();
        }
        return commandLine;
    }
}