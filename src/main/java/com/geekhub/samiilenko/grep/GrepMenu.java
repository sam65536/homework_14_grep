package com.geekhub.samiilenko.grep;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class GrepMenu {
    private Options options = new Options();

    public GrepMenu() {
        Option option1 = new Option("w", "word", true, "find word.");
        option1.setArgs(Option.UNLIMITED_VALUES);
        Option option2 = new Option("r", "regex", true, "find regex.");
        Option option3 = new Option("f", "file", true, "file source.");
        Option option4 = new Option("u", "url", true, "url source.");
        Option option5 = new Option("h", "help", false, "show help.");
        options.addOption(option1);
        options.addOption(option2);
        options.addOption(option3);
        options.addOption(option4);
        options.addOption(option5);
    }

    public Options getOptions() {
        return options;
    }

    public void showHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar grep.jar", "Options", options, "-- HELP --", true);
    }
}