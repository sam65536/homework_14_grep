package com.geekhub.samiilenko.grep;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

public class GrepRequest {

    private String[] text;
    private String source;
    private boolean regexp;
    private boolean help;

    public String[] getText() {
        return text;
    }

    public String getSource() {
        return source;
    }

    public boolean isRegexp() {
        return regexp;
    }

    public boolean isHelp() {
        return help;
    }

    public void generateRequest(String[] arguments) throws InvalidParametersException {
        CommandLineValidator validator = new CommandLineValidator();
        CommandLine cmd;
        if (validator.isValid(arguments)) {
            cmd = validator.getCommandLineFromString(arguments);
        } else {
            throw new InvalidParametersException();
        }
        text = (cmd.hasOption("w")) ? cmd.getOptionValues("w") : cmd.getOptionValues("r");
        source = (cmd.hasOption("f")) ? cmd.getOptionValue("f") : cmd.getOptionValue("u");
        regexp = (cmd.hasOption("r")) ? true : false;
        help = (cmd.hasOption("h")) ? true : false;
    }
}