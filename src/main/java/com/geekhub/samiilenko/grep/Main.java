package com.geekhub.samiilenko.grep;

import java.util.List;

public class Main {
    public static void main(String[] args) throws InvalidParametersException, TextLoaderException {
        GrepRequest request = new GrepRequest();
        request.generateRequest(args);
        if (request.isHelp()) {
            new GrepMenu().showHelp();
        } else {
            TextLoader loader = new TextLoader();
            List<String> lines = loader.getLines(request);
            lines.forEach(System.out::println);
        }
    }
}