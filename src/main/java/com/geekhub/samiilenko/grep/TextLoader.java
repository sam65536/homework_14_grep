package com.geekhub.samiilenko.grep;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextLoader {

    public List<String> getLines(GrepRequest request) throws TextLoaderException, InvalidParametersException {
        String[] text = request.getText();
        String source = request.getSource();
        List<String> lines;
        List<String> result;
        File file = new File(source);
        lines = file.exists() ? readLinesfromFile(source) : readLinesfromURL(source);
        result = request.isRegexp() ? parseLinesByRegExp(lines, text[0]) : parseLinesByWords(lines, text);
        return result;
    }

    private List<String> parseLinesByWords(List<String> lines, String[] words) {
        List<String> result = new ArrayList<>();
        Set<Integer> linesNums = new HashSet<>();
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < lines.size(); j++) {
                if (linesNums.contains(j)) {
                    continue;
                }
                if (lines.get(j).toLowerCase().contains(words[i].toLowerCase())) {
                    result.add(lines.get(j));
                    linesNums.add(j);
                }
            }
        }
        return result;
    }

    private List<String> parseLinesByRegExp(List<String> lines, String regexp) throws InvalidParametersException {
        List<String> result = new ArrayList<>();
        Pattern pattern;
        try {
            pattern = Pattern.compile(regexp);
        } catch (Exception e) {
            throw new InvalidParametersException();
        }
        for (int i = 0; i < lines.size(); i++) {
            Matcher matcher = pattern.matcher(lines.get(i));
            if (matcher.find()) {
                result.add(lines.get(i));
            }
        }
        return result;
    }

    private List<String> readLinesfromFile(String source) throws TextLoaderException {
        List<String> result;
        File file = new File(source);
        try {
            result = FileUtils.readLines(file, "UTF-8");
        } catch (IOException e) {
            throw new TextLoaderException();
    }
        return result;
    }

    private List<String> readLinesfromURL(String source) throws TextLoaderException {
        URL url;
        try {
            url = new URL(source);
        } catch (MalformedURLException e) {
            throw new TextLoaderException();
        }
        List<String> result = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                result.add(currentLine);
            }
        return result;
            } catch (IOException e) {
            throw new TextLoaderException();
        }
    }
}