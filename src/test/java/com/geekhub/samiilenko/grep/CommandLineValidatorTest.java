package com.geekhub.samiilenko.grep;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommandLineValidatorTest {
    @Test
    public void isValid() throws Exception {
        CommandLineValidator validator = new CommandLineValidator();
        String args1[] = new String[] {"-w", "Hello World", "-f", "/home/user/Desktop/song.txt"};
        String args2[] = new String[] {"-u", "Hello World", "-f", "/home/user/Desktop/song.txt"};
        assertTrue(validator.isValid(args1));
        assertFalse(validator.isValid(args2));
    }
}