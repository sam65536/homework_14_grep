package com.geekhub.samiilenko.grep;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.WindowsFakeFileSystem;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TextLoaderTest {
    private static FakeFtpServer fakeFTP = new FakeFtpServer();

    @BeforeClass
    public static void startAndSetUpFakeFTP() {
        UserAccount userAccount = new UserAccount("test", null, "c:\\data");
        userAccount.setAccountRequiredForLogin(false);
        userAccount.setPasswordCheckedDuringValidation(false);
        userAccount.setPasswordRequiredForLogin(false);
        fakeFTP.addUserAccount(userAccount);
        WindowsFakeFileSystem fileSystem = new WindowsFakeFileSystem();
        fileSystem.add(new DirectoryEntry("c:\\data"));
        fileSystem.add(new FileEntry("c:\\data\\file1.txt",
                "ERROR 1\n" +
                "fine\n" +
                "fine\n" +
                "ERROR 2\n" +
                "fine\n" +
                "customer Marco(id=12345) has been updated successfully\n" +
                "fine extract\n" +
                "fine double extract"));
        fakeFTP.setFileSystem(fileSystem);
        fakeFTP.start();
    }

    @AfterClass
    public static void tearDown() {
        fakeFTP.stop();
    }

    @Test
    public void getLines() throws Exception {
        TextLoader textLoader = new TextLoader();
        String args[] = new String[] {"--word", "fine", "-u", "ftp://test@localhost/file1.txt"};
        List<String> result =  Arrays.asList(new String[]{"fine", "fine", "fine", "fine extract", "fine double extract"});
        GrepRequest request = new GrepRequest();
        request.generateRequest(args);
        assertEquals(result, textLoader.getLines(request));
    }
}